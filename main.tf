# Create an EC2 key pair
resource "aws_key_pair" "myssh" {
  key_name   = var.ssh_keyname  #"myssh"
  public_key = file("~/.ssh/id_rsa.pub")
}

# Create a security group that allows SSH access

resource "aws_security_group" "my_sg_ssh" {
  name        = var.sg_name
  description = "Allow SSH access"

  ingress = [{
    description = "Allow SSH"
    from_port   = var.from_port_ingress
    to_port     = var.to_port_ingress
    protocol    = var.protocol_ingress
    cidr_blocks = var.cidr_blocks_ingress
    ipv6_cidr_blocks = var.ipv6_cidr_blocks_ingress
    prefix_list_ids = var.prefix_list_ids_ingress
    security_groups = var.security_groups_ingress
    self = var.self_ingress

  }]

  egress = [{
    description = "Allow connection to any internet service"
    from_port   = var.from_port_egress
    to_port     = var.to_port_egress
    protocol    = var.protocol_egress
    cidr_blocks = var.cidr_blocks_egress
    ipv6_cidr_blocks = var.ipv6_cidr_blocks_egress
    prefix_list_ids = var.prefix_list_ids_egress
    security_groups = var.security_groups_egress
    self = var.self_egress

  }]
}

# Create an EC2 instance
resource "aws_instance" "my_ec2" {
  ami           = "ami-0ff8a91507f77f867"
  instance_type = "t2.micro"
  key_name      = aws_key_pair.myssh.key_name
  security_groups = [aws_security_group.my_sg_ssh.name]
  tags = {
    "Name":"myvm"
  }
}
