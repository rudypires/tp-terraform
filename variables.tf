variable "aws_region" {
  type = string
  default = "us-east-1"
  description = "region of AWS deployment"
}

variable "aws_key" {
  type = string
  description = "key of AWS deployment"
}

variable "aws_secret" {
  type = string
  description = "secret key of AWS deployment"
}

variable "aws_token" {
  type = string
  description = "token of AWS deployment"
}

variable "ssh_keyname" {
  type = string
  description = "name of ssh key"
}

variable "sg_name" {
  type = string
  description = "name of security group"
}

# Define variables for ingress and egress rules
variable "from_port_ingress" {
  type = number
}

variable "to_port_ingress" {
  type = number
}

variable "protocol_ingress" {
  type = string
}

variable "cidr_blocks_ingress" {
  type = list(string)
}

variable "ipv6_cidr_blocks_ingress" {
  type = list(string)
}

variable "prefix_list_ids_ingress" {
  type = list(string)
}

variable "security_groups_ingress" {
  type = list(string)
}

variable "self_ingress" {
  type = bool
}

variable "from_port_egress" {
  type = number
}

variable "to_port_egress" {
  type = number
}

variable "protocol_egress" {
  type = string
}

variable "cidr_blocks_egress" {
  type = list(string)
}

variable "ipv6_cidr_blocks_egress" {
  type = list(string)
}

variable "prefix_list_ids_egress" {
  type = list(string)
}

variable "security_groups_egress" {
  type = list(string)
}

variable "self_egress" {
  type = bool
}
